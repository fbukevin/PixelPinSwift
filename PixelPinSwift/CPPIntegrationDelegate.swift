//
//  CPPIntegrationDelegate.swift
//  PixelPin
//
//  Created by Veck on 2017/2/3.
//  Copyright © 2017年 KPMG. All rights reserved.
//

import Foundation

class CPPIntegrationDelegate: UIResponder, PPIntegrationDelegate {

    func ppIntegration(_ integration: PPIntegration, errorDidOccur message: String, withTitle title: String) {
        // Handle your error here in some way, the default is just an Alert View
        let alert = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
        alert.show()
    }
    
    func clientData(_ integration: PPIntegration) -> PPClientData {
        let dict = PPClientData()
        dict.appName = "sanity.PixelPinSwift"
        dict.clientId = "JT3KMIBJGP0VVNXUF12FBJYUFDZXUO"
        dict.secret = "t|nr3jTu3JYQtw.7fJBby.RyTac8cn"
        dict.key = "b/7qhwyo64CbvdA+UTBB54JuUHGFX69I5wZNqy7uSKc="
        return dict
    }
    
    func loginFailed(_ integration: PPIntegration) {
        // Handle failure in this method, replacing these contents if you wish
        print("Login failed!")
    }
    
    func loginSuccessful(_ integration: PPIntegration) {
        // Handle success in this method, replacing these contents if you wish
        print("Login successfully!")
    }
}
