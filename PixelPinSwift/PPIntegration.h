//
//  PPIntegration.h
//  PPIntegration
//
//  Created by Luke Briner on 08/04/2014.
//  Copyright (c) 2014 PixelPin. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <Security/Security.h>
#import <CommonCrypto/CommonCryptor.h>
//#import "PPClientData.h"


@interface PPClientData : NSObject
@property(nonatomic, strong) NSString* clientId;
@property(nonatomic, strong) NSString* secret;
@property(nonatomic, strong) NSString* key;
@property(nonatomic, strong) NSString* appName;
@end

@protocol PPIntegrationDelegate;

@interface PPIntegration : NSObject

@property(nonatomic, strong) NSString* token;
@property (nonatomic, retain) NSString *result;

+ (PPIntegration*) getInstance;
- (void) loginWithPixelPin:(NSString *) emailAddress;
- (BOOL) handleReturnAuth: (NSURL*) url;
- (BOOL) isReturnUrlASuccessfulLogin: (NSURL *) url;
- (BOOL) isReturnUrlPreAuth: (NSURL *) url;

// Delegate methods
@property (nonatomic, strong) id<PPIntegrationDelegate> delegate;   // Retain reference

@end

@protocol PPIntegrationDelegate <NSObject>

- (void)ppIntegration:(PPIntegration*)integration
        errorDidOccur:(NSString*)message withTitle:(NSString*)title;
- (void)loginSuccessful:(PPIntegration*)integration;
- (void)loginFailed:(PPIntegration*)integration;
- (PPClientData*)clientData:(PPIntegration*) integration;
@end


