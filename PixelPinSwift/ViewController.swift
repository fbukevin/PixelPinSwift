//
//  ViewController.swift
//  PixelPinSwift
//
//  Created by Veck on 2017/2/3.
//  Copyright © 2017年 Sanity. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pp = PPIntegration.getInstance()
        pp?.login(withPixelPin: "fbukevin@gmail.com")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

