//
//  CPPIntegrationDelegate.h
//  PayIt
//
//  Created by Luke Briner on 08/04/2014.
//  Copyright (c) 2014 PixelPin Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PPIntegration.h"

@interface CPPIntegrationDelegate : UIResponder <PPIntegrationDelegate>

    - (void)ppIntegration:(PPIntegration*)integration
            errorDidOccur:(NSString*)message withTitle:(NSString*)title;
    - (PPClientData*)clientData:(PPIntegration *)integration;
    
@end
