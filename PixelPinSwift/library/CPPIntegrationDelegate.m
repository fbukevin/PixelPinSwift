//
//  CPPIntegrationDelegate.m
//
//  Created by Luke Briner on 08/04/2014.
//  Copyright (c) 2014 PixelPin Ltd. All rights reserved.
//

#import "CPPIntegrationDelegate.h"
#import "CFailedLoginViewController.h"		// Your failed view controller goes here
#import "CSuccessfulLoginViewController.h"	// Your successful view controller goes here

@implementation CPPIntegrationDelegate


- (void)ppIntegration:(PPIntegration*)integration
        errorDidOccur:(NSString*)message withTitle:(NSString*) title
{
    // Handle your error here in some way, the default is just an Alert View
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (PPClientData*)clientData:(PPIntegration *)integration
{
    PPClientData* dict = [[PPClientData alloc] init];
    dict.appName = @"com.yourcompany.yourapp”;
    dict.clientId = @“Your “client id in here;
    dict.secret = @“Your “client secret in here;
    dict.key = @“Your “client key in here;
    return dict;
}

- (void) loginFailed:(PPIntegration*)integration {
    // Handle failure in this method, replacing these contents if you wish
    UINavigationController *navigationController = (UINavigationController *)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    
    CFailedLoginViewController *controller = (CFailedLoginViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"FailedLogin"];
    [navigationController pushViewController:controller animated:YES];
}

- (void) loginSuccessful:(PPIntegration*)integration {
    // Handle success in this method, replacing these contents if you wish
    UINavigationController *navigationController = (UINavigationController *)[[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] nextResponder];;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle: nil];
    
    CSuccessfulLoginViewController *controller = (CSuccessfulLoginViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"SuccessfulLogin"];
    [navigationController pushViewController:controller animated:YES];
}

@end
